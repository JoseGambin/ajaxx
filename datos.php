<?php
if(isset($_POST["nombre"])) {
    $nombre     = $_POST["nombre"];
    $msg_error  = "";
    $sucess     =true;
    $data       = array();

    try {
        $con = new PDO('mysql:host=localhost;dbname=universidad', 'root', '');
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $con->exec("set names utf8");
        $query = "SELECT nombre, apellido_1, apellido_2 FROM alumno WHERE nombre LIKE :nombre";
        $stmt = $con->prepare($query);
        $stmt->bindValue(":nombre", "%" . $nombre . "%");
        $stmt->execute();
        $arr = array();

        while ($registro = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $data[] = $registro['nombre']." ". $registro['apellido_1']." ".$registro['apellido_2'];
        }

    } catch (PDOException $e) {
        $msg_error = "ERROR DE CONEXION";
        $sucess = false;
    }


    $json = array(
        "success" => $sucess,
        "msg" => $msg_error,
        "data" => $data
    );

    echo json_encode($json);
}
?>